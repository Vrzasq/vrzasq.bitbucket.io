
function lol() {
    $.get({
        url: "assets/words.txt",
        success: function (data) {
            var storedSelector = $("#testID");
            var el = data.split("\n");
            storedSelector.text(getRandomWord(el));
        }
    });
}

function getRandomWord(wordArray) {
    var randomIndex = Math.round(Math.random() * wordArray.length);
    return wordArray[randomIndex];
}